# README #

Aplikacja symulująca działanie aplikacji bankowej (np. platformy internetowej).
Funkcjonalności:
Tworzenie Rachunków
Wykonywania przelewów
Zapisywanie Historii Operacji
Tworzenie Lokat
Oprocentowanie Lokat
itd.itp.

### Po co?
Napisana do zaliczenia projektu z C#, wcześniejszą aplikacją była napisana w technologii ASP.NET MVC -
 w związku z komentarzem w xlsu:
Użyty scaffolding - wszystko zostało wygenerowane.  Widać własne pola i zmiany w metodach, jednak proszę o zbudowanie prostego projektu demonstrującego zastosowanie wymaganych elementów - w innej technologii niż ASP.NET MVC.

Napisałem aplikację konsolową

### Jak uruchomić?
Pobrać całe solution f5 i go

### Co w niej zostało zawarte?
Wszystkie wymagania z XLS

3 klasy [0-1]	- jest więcej

Dziedziczenie [0-1]	- Lokata po Rachunku

Polimorfizm [0-1]	- Metoda UtworzNowyRachunek z klas Lokata I Rachunek

Wyjątki [0-1]	        - HistoriaOperacji - Metoda ZapiszDoPliku IOException I PathTooLongException

Propercje [0-1]	        - Dużo propercji

Enumy [0-1]	        - Dużo enumów

Interfejsy [0-2]	- Przykładowo na Klasie Rachunki (lista<Rachunek>) Rachunki : IEnumerable<Rachunek>, IList<Rachunek>

Enkapsulacja [0-1]	- proste przykłady (protected int getID() w Klasie Rachunek - używany potem w Lokacie, private int getID() tylko w przelewach)

Kolekcja [0-1]          - Dużo kolekcji listy, słowniki

Dodatkowo parę ciekawych rzeczy typu FolderBrowserDialog czy Timery do naliczania procent na Lokacie
Resztę pozostawiam do oceny.