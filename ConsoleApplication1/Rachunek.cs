﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    public class Rachunek 
    {
        private static int ID = 1;
        public int AccId { get; set; }
        public string AccNumber { get; set; }
        public double AccBalance { get; set; }
        public DateTime AccOpenDate { get; set; }
        public AccType AccType { get; set; }
        public virtual string UtworzNowyRachunek()
        {

            var rnd = new Random(Guid.NewGuid().GetHashCode());
            this.AccNumber = rnd.Next(100000000,444444444).ToString();
            return this.AccNumber;

        }
        public bool TryPoprawSaldo(double amount)
        {
            if (this.AccBalance + amount >= 0)
            {
                this.AccBalance = this.AccBalance + amount;
                return true;
            }
            else
            return false; 


        }
        public Rachunek(AccType acc_type)
        {

            this.AccNumber = UtworzNowyRachunek();
            this.AccBalance = 100.0;
            this.AccOpenDate = DateTime.Now;
            this.AccType = acc_type;
            this.AccId = getID();

        }

        protected int getID()
        {
            return ID++;
        }
       

    }


    class Lokata : Rachunek
    {
        public DepositType DepositType { get; set; }
        public string DepositAccNumber { get; set; }



        public Lokata(AccType acc_type) : base(acc_type)
        {
            this.AccNumber = UtworzNowyRachunek();
            this.AccBalance = 0;
            this.AccOpenDate = DateTime.Now;
            this.AccType = acc_type;
            this.AccId = getID();
        }

        public override string  UtworzNowyRachunek()
        {

            var rnd = new Random(Guid.NewGuid().GetHashCode());
            this.AccNumber = rnd.Next(555555555, 999999999).ToString();
            return this.AccNumber;

        }


    }

    enum DepositType
    {
        Trzymiesieczna,
        Polroczna,
        Roczna
    }

    public enum AccType
    {
        ROR,
        Lokata
    }


    public class Rachunki : IEnumerable<Rachunek>, IList<Rachunek>
    {

        public List<Rachunek> _Rachunki;

        public int Count
        {
            get
            {
                return ((IList<Rachunek>)_Rachunki).Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return ((IList<Rachunek>)_Rachunki).IsReadOnly;
            }
        }

        public Rachunek this[int index]
        {
            get
            {
                return ((IList<Rachunek>)_Rachunki)[index];
            }

            set
            {
                ((IList<Rachunek>)_Rachunki)[index] = value;
            }
        }

        public Rachunki(Rachunek rachunek)
        {
            this._Rachunki = new List<Rachunek>();

            _Rachunki.Add(rachunek);

        }

        public void WyswietlRachunki(AccType acc_type)
        {

            Console.WriteLine("Nr Rachunku \t\tSaldo \t\tData Utworzenia");
            foreach (var item in this._Rachunki)
            {
                if (item.AccType == acc_type)
                {
                    Console.WriteLine(item.AccNumber + "\t\t" + item.AccBalance + "\t\t" + item.AccOpenDate);
                }
            }


        }
        public void WyswietlRachunki(bool x,AccType acc_type)
        {
            int i = 1;






     
                Console.WriteLine("ID \tNr Rachunku \t\tSaldo \t\tData Utworzenia");
                foreach (var item in this._Rachunki)
                {
                    if (item.AccType == acc_type)
                    {
                        Console.WriteLine(i + ". \t" + item.AccNumber + "\t\t" + item.AccBalance + "\t\t" + item.AccOpenDate);
                        i++;
                    }
                }
         



        

        }
        public void DodajRachunek(AccType acc_type)
        {
            Rachunek tmp = new Rachunek(acc_type);
            this._Rachunki.Add(tmp);


        }


        public IEnumerator<Rachunek> GetEnumerator()
        {
            return this._Rachunki.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this._Rachunki.GetEnumerator();
        }

        public int IndexOf(Rachunek item)
        {
            return ((IList<Rachunek>)_Rachunki).IndexOf(item);
        }

        public void Insert(int index, Rachunek item)
        {
            ((IList<Rachunek>)_Rachunki).Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            ((IList<Rachunek>)_Rachunki).RemoveAt(index);
        }

        public void Add(Rachunek item)
        {
            ((IList<Rachunek>)_Rachunki).Add(item);
        }

        public void Clear()
        {
            ((IList<Rachunek>)_Rachunki).Clear();
        }

        public bool Contains(Rachunek item)
        {
            return ((IList<Rachunek>)_Rachunki).Contains(item);
        }

        public void CopyTo(Rachunek[] array, int arrayIndex)
        {
            ((IList<Rachunek>)_Rachunki).CopyTo(array, arrayIndex);
        }

        public bool Remove(Rachunek item)
        {
            return ((IList<Rachunek>)_Rachunki).Remove(item);
        }


    }


}
