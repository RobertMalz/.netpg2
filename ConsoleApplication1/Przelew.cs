﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    public class Przelew
    {

        private static int ID = 1;
        public int TrnId { get; set; }
        public string TrnOwnAccNumber { get; set; }
        public DateTime TrnDate { get; set; }
        public string BnfAccNumber { get; set; }
        public string BnfName { get; set; }
        public double TrnAmount { get; set; }
        private string TrnState = "Niezrealizowany";


        public bool WykonajPrzelew(Rachunki listaRachunkow, HistoriaOperacji historiaOperacji)
        {

            Rachunek targetAccount = new Rachunek(AccType.ROR);

            if (this.TrnOwnAccNumber == null || this.BnfAccNumber == null)
            {
                PrzelewErrorBuilder("Wypełnij wszystkie pola!");
                return false;
            }

            if (this.TrnAmount <= 0)
            {
                PrzelewErrorBuilder("Kwota musi być większa od 0");
                return false;
            }

            Rachunek sourceAccount = listaRachunkow.First(o => o.AccNumber == this.TrnOwnAccNumber);

            if (!listaRachunkow.Any(p => p.AccNumber == this.BnfAccNumber))
            {
                targetAccount.AccNumber = BnfAccNumber;

            }
            else
            {
                targetAccount = listaRachunkow.First(o => o.AccNumber == this.BnfAccNumber);
            }



            if (sourceAccount == null || targetAccount == null)
            {
                PrzelewErrorBuilder("Problem z rachunkiem! Krytyczny ERROR systemu Skontaktuj się z HotLine xD");
                return false;
            }

            if (this.TrnOwnAccNumber == this.BnfAccNumber)
            {
                PrzelewErrorBuilder("Rachunek źródłowy i docelowy nie może być taki sam");
                return false;
            }



            if (!sourceAccount.TryPoprawSaldo(-TrnAmount))
            {

                PrzelewErrorBuilder("Za mało siana na rachunku: " + sourceAccount.AccNumber);
                return false;
            }



            if (targetAccount.AccOpenDate != null)
            {
                targetAccount.TryPoprawSaldo(TrnAmount);
            }


            this.TrnState = "Zrealizowany";
            if (BnfName == null)
            {
                BnfName = "Przelew na rachunek własny";
            }
            TrnId = getID();
            TrnDate = System.DateTime.Now;
            historiaOperacji.Add(this);
            Console.WriteLine("Środki przelane.");
            return true;

        }

        public bool PrzelewErrorBuilder(string Error)
        {
            Console.WriteLine("Przelew nie został zrealizowany: " + Error);
            return false;
        }

        private int getID()
        {
            return ID++;
        }



    }



}
