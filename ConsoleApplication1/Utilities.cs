﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace ConsoleApplication1
{
    class Utilities
    {

        public static int ValidateInput(int[] a)
        {
            while (true)
            {
                var c = Console.ReadKey(true);

                if (Char.IsDigit(c.KeyChar) && a.Contains((int)Char.GetNumericValue(c.KeyChar)))
                {
                    return (int)Char.GetNumericValue(c.KeyChar);
                }

            }



        }
        public static double ValidateInput()
        {
            var input = Console.ReadLine();
            double amount;

            bool isDouble = Double.TryParse(input, out amount);
            if (isDouble)
            {

                return amount;
            }
            else
            {
                Console.WriteLine("Podaj poprawną wartość:");
                amount = ValidateInput();
            }


            return amount;



        }
        public static void PokazMenuRachunkow()
        {
            Console.WriteLine("1. Utwórz nowy rachunek");
            Console.WriteLine("2. Sortuj po numerze rachunku");
            Console.WriteLine("3. Sortuj po saldzie");
            Console.WriteLine("4. Sortuj po dacie utworzenia");
            Console.WriteLine("5. Powrót do Menu");


        }
        public static void PokazMenuPrzelewow()
        {

            Console.WriteLine("1. Wykonaj przelew na rachunek własny");
            Console.WriteLine("2. Wykonaj przelew na rachunek w innym 'banku'");
            Console.WriteLine("3. Powrót do Menu");

        }
        public static void PokazMenuPrzelewowKrajowych(Przelew Trn)
        {

            Console.WriteLine("Przelew krajowy");
            Console.WriteLine("1. Wybierz rachunek źródłowy");
            Console.WriteLine("2. Wpisz rachunek docelowy");
            Console.WriteLine("3. Wpisz kwotę");
            Console.WriteLine("4. Uzupełnij dane odbiorcy");
            Console.WriteLine("5. Wykonaj przelew");
            Console.WriteLine("9. Powrót");
            Console.WriteLine("##########################################\n\n");
            Console.WriteLine("Dane Przelewu");
            Console.WriteLine("Rachunek źródłowy: \t\t{0}", Trn.TrnOwnAccNumber);
            Console.WriteLine("Rachunek docelowy: \t\t{0}", Trn.BnfAccNumber);
            Console.WriteLine("Kwota: \t\t\t\t{0}", Trn.TrnAmount);
            Console.WriteLine("Dane odbiorcy: \t\t\t{0}", Trn.BnfName);







        }
        public static bool ValidateAccNumber(string Acc)
        {
          
            Regex r = new Regex(@"^\d{9}$");



            if (!r.IsMatch(Acc))
            {
                Console.WriteLine("Rachunek musi się składać z 9 cyfr");
                Console.ReadKey(true);
                return false;

            }
            else
            {
                return true;
            }


        }
        public static void PokazMenuLokat()
        {

            Console.WriteLine("1. Utwórz nową lokatę");
            Console.WriteLine("2. Zerwij lokatę");
            Console.WriteLine("3. Powrót do Menu");





        }

       


        public static void ChangeDepositBalance(object source, System.Timers.ElapsedEventArgs e, Rachunki listaRachunkow, DepositType dep_type)
        {

            var rachunki = listaRachunkow.Where(s => s.AccType == AccType.Lokata);
                       

            List<Lokata> lokaty = rachunki.Cast<Lokata>().ToList();



            foreach (var item in lokaty)
            {
                if (item.DepositType == dep_type)
                {

                    var obj = listaRachunkow.First(x => x.AccNumber == item.AccNumber);
                    if (obj != null) obj.AccBalance = Math.Round(obj.AccBalance + (obj.AccBalance * 0.01), 2);
                    
                }
            }




        }

    }
}
