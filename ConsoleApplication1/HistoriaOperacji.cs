﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;


namespace ConsoleApplication1
{
    
    public class HistoriaOperacji : IList<Przelew>
    {
        public List<Przelew> _HistoriaOperacji;

        public HistoriaOperacji()
        {
            _HistoriaOperacji = new List<Przelew>();
        }


        public void WyswietlHistorieOperacji()
        {
            Console.WriteLine("ID\tRachunek źródłowy\tRachunek docelowy\tKwota\tData realizacji przelewu\tDane odbiorcy");
            if (_HistoriaOperacji.Count == 0)
            {
                Console.WriteLine("BRAK OPERACJI");
                return;
            }
            foreach (var item in _HistoriaOperacji)
            {
                Console.WriteLine("{0}\t{1}\t\t{2}\t\t{3}\t{4}\t\t{5}", item.TrnId, item.TrnOwnAccNumber, item.BnfAccNumber, item.TrnAmount, item.TrnDate, item.BnfName);
            }


        }

        public bool ZapiszDoPliku()
        {
            string output = "";
            output = "ID\tRachunek źródłowy\tRachunek docelowy\tKwota\tData realizacji przelewu\tDane odbiorcy";
            foreach (var item in _HistoriaOperacji)
            {
                output= output + Environment.NewLine+ string.Format("{0}\t{1}\t\t{2}\t\t{3}\t{4}\t\t{5}", item.TrnId, item.TrnOwnAccNumber, item.BnfAccNumber, item.TrnAmount, item.TrnDate, item.BnfName);
            }


            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                var folderName = fbd.SelectedPath;



                try
                {
                    using (System.IO.StreamWriter outputFile = new System.IO.StreamWriter(folderName + @"\HistoriaOperacji.txt", true))
                    {
                        outputFile.WriteLine(output);


                        return true;
                    }
                }
                catch (PathTooLongException e)
                {
                    Console.WriteLine("Zbyt długa nazwa pliku/scieżki: {0}", e.Message);
                }
                catch (IOException e)
                {
                    Console.WriteLine("Zapis do pliku się nie powiódł: {0}" , e.Message);
                    throw;
                }
                

            }

            return false;

        }



        public Przelew this[int index]
        {
            get
            {
                return ((IList<Przelew>)_HistoriaOperacji)[index];
            }

            set
            {
                ((IList<Przelew>)_HistoriaOperacji)[index] = value;
            }
        }

        public int Count
        {
            get
            {
                return ((IList<Przelew>)_HistoriaOperacji).Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return ((IList<Przelew>)_HistoriaOperacji).IsReadOnly;
            }
        }

        public void Add(Przelew item)
        {
            ((IList<Przelew>)_HistoriaOperacji).Add(item);
        }

        public void Clear()
        {
            ((IList<Przelew>)_HistoriaOperacji).Clear();
        }

        public bool Contains(Przelew item)
        {
            return ((IList<Przelew>)_HistoriaOperacji).Contains(item);
        }

        public void CopyTo(Przelew[] array, int arrayIndex)
        {
            ((IList<Przelew>)_HistoriaOperacji).CopyTo(array, arrayIndex);
        }

        public IEnumerator<Przelew> GetEnumerator()
        {
            return ((IList<Przelew>)_HistoriaOperacji).GetEnumerator();
        }

        public int IndexOf(Przelew item)
        {
            return ((IList<Przelew>)_HistoriaOperacji).IndexOf(item);
        }

        public void Insert(int index, Przelew item)
        {
            ((IList<Przelew>)_HistoriaOperacji).Insert(index, item);
        }

        public bool Remove(Przelew item)
        {
            return ((IList<Przelew>)_HistoriaOperacji).Remove(item);
        }

        public void RemoveAt(int index)
        {
            ((IList<Przelew>)_HistoriaOperacji).RemoveAt(index);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IList<Przelew>)_HistoriaOperacji).GetEnumerator();
        }
    }

}
