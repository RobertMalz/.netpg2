﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Timers;



namespace ConsoleApplication1
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {

            

            var listaRachunkow = new Rachunki(new Rachunek(AccType.ROR));

            var historiaOperacji = new HistoriaOperacji();


            #region TimerConf
            System.Timers.Timer Timer3M = new System.Timers.Timer();
            Timer3M.Elapsed += (sender, e) => Utilities.ChangeDepositBalance(sender, e, listaRachunkow, DepositType.Trzymiesieczna);
            Timer3M.Interval = 3000;
            Timer3M.Enabled = true;



            System.Timers.Timer Timer6M = new System.Timers.Timer();
            Timer6M.Elapsed += (sender, e) => Utilities.ChangeDepositBalance(sender, e, listaRachunkow, DepositType.Polroczna);
            Timer6M.Interval = 6000;
            Timer6M.Enabled = true;


            System.Timers.Timer Timer12M = new System.Timers.Timer();
            Timer12M.Elapsed += (sender, e) => Utilities.ChangeDepositBalance(sender, e, listaRachunkow, DepositType.Roczna);
            Timer12M.Interval = 12000;
            Timer12M.Enabled = true;
            #endregion




            bool czyZostac = true;
            while (czyZostac)
            {
                Console.Clear();
                Console.WriteLine("1. Twoje Rachunki");
                Console.WriteLine("2. Wykonaj przelew");
                Console.WriteLine("3. Wyświetl Historię Operacji");
                Console.WriteLine("4. Twoje Lokaty");
                Console.WriteLine("5. Wyjście");

                int i = Utilities.ValidateInput(new int[] { 1, 2, 3, 4, 5 });

                Console.WriteLine(i);

                switch (i) //TODO: Dodać logowanie?stworzyć jakąś bazę danych? - dużo roboty  -jeżeli będzie taka konieczność
                {
                    case (int)Menu.Rachunki:
                        {
                            #region Rachunki
                            var RachunkiMenu = true;
                            string sorting = "";
                            while (RachunkiMenu)
                            {
                                Console.Clear();
                                listaRachunkow.WyswietlRachunki(AccType.ROR);
                                Utilities.PokazMenuRachunkow();
                                var temp = Utilities.ValidateInput(new int[] { 1, 2, 3, 4, 5 });
                                switch (temp)
                                {

                                    case 1:
                                        {
                                            listaRachunkow.DodajRachunek(AccType.ROR);
                                            break;
                                        }
                                    case 2:
                                        {
                                            if (sorting == "AccNumber")
                                            {
                                                listaRachunkow._Rachunki.Sort((y, x) => x.AccNumber.CompareTo(y.AccNumber));
                                                sorting = "";
                                            }
                                            else
                                            {
                                                listaRachunkow._Rachunki.Sort((x, y) => x.AccNumber.CompareTo(y.AccNumber));
                                                sorting = "AccNumber";
                                            }
                                            break;
                                        }
                                    case 3:
                                        {
                                            if (sorting == "AccBalance")
                                            {
                                                listaRachunkow._Rachunki.Sort((y, x) => x.AccBalance.CompareTo(y.AccBalance));
                                                sorting = "";
                                            }
                                            else
                                            {
                                                listaRachunkow._Rachunki.Sort((x, y) => x.AccBalance.CompareTo(y.AccBalance));
                                                sorting = "AccBalance";
                                            }
                                            break;
                                        }
                                    case 4:
                                        {
                                            if (sorting == "AccDate")
                                            {
                                                listaRachunkow._Rachunki.Sort((y, x) => x.AccOpenDate.CompareTo(y.AccOpenDate));
                                                sorting = "";
                                            }
                                            else
                                            {
                                                listaRachunkow._Rachunki.Sort((x, y) => x.AccOpenDate.CompareTo(y.AccOpenDate));
                                                sorting = "AccDate";
                                            }
                                            break;
                                        }

                                    case 5:
                                        {
                                            RachunkiMenu = false;
                                            break;
                                        }

                                }
                            }

                            break;
                            #endregion
                        }

                    case (int)Menu.Przelewy: //TODO: zapomniałem dodać tytuł przelewu uniwersalnym polem są dane odbiorcy
                        {
                            #region Przelewy
                            var PrzelewyMenu = true;
                            while (PrzelewyMenu)
                            {
                                Console.Clear();
                                Utilities.PokazMenuPrzelewow();

                                var temp = Utilities.ValidateInput(new int[] { 1, 2, 3 });

                                switch (temp)
                                {
                                    case 1:
                                        {
                                            #region PrzelewNaRachunekWłasny
                                            Przelew Transfer = new Przelew();
                                            Console.Clear();
                                            Console.WriteLine("Przelew na rachunek własny.");
                                            Console.WriteLine("##########################################");
                                            Console.WriteLine("Podaj rachunek źródłowy:");
                                            int[] inputArray = new int[listaRachunkow.Where(Acc => Acc.AccType == AccType.ROR).Count()];
                                            for (int y = 1; y < listaRachunkow.Where(Acc => Acc.AccType == AccType.ROR).Count() + 1; y++)
                                            {
                                                inputArray[y - 1] = y;
                                            }
                                            listaRachunkow.WyswietlRachunki(true, AccType.ROR);
                                            var input = Utilities.ValidateInput(inputArray);
                                            Transfer.TrnOwnAccNumber = listaRachunkow[input - 1].AccNumber;


                                            Console.WriteLine("Wybrałeś rachunek: " + Transfer.TrnOwnAccNumber);
                                            Console.WriteLine("##########################################");
                                            Console.WriteLine("Wybierz rachunek docelowy:");
                                            listaRachunkow.WyswietlRachunki(true, AccType.ROR);
                                            input = Utilities.ValidateInput(inputArray);
                                            Transfer.BnfAccNumber = listaRachunkow[input - 1].AccNumber;
                                            Console.WriteLine("Wybrałeś rachunek: " + Transfer.BnfAccNumber);
                                            Console.WriteLine("##########################################");
                                            Console.WriteLine("Podaj kwotę:");
                                            Transfer.TrnAmount = Utilities.ValidateInput();
                                            Console.Clear();
                                            Console.WriteLine("Przelew na rachunek własny.");
                                            Console.WriteLine("Rachunek źródłowy: " + Transfer.TrnOwnAccNumber);
                                            Console.WriteLine("Rachunek docelowy: " + Transfer.BnfAccNumber);
                                            Console.WriteLine("Kwota: " + Transfer.TrnAmount);
                                            Console.WriteLine("1. Realizuj przelew.");
                                            Console.WriteLine("2. Wpisz dane ponownie.");
                                            input = Utilities.ValidateInput(new int[] { 1, 2 });
                                            if (input == 1)
                                            {
                                                Transfer.WykonajPrzelew(listaRachunkow, historiaOperacji);
                                                Console.ReadKey();
                                            }


                                            break;
                                            #endregion
                                        }
                                    case 2:
                                        {

                                            #region PrzelewNaRachunekZewnetrzny
                                            Przelew TransferKrajowy = new Przelew();
                                            var przelewMenu = true;
                                            int[] inputArray = new int[listaRachunkow.Where(Acc => Acc.AccType == AccType.ROR).Count()];
                                            while (przelewMenu)
                                            {
                                                Console.Clear();
                                                Utilities.PokazMenuPrzelewowKrajowych(TransferKrajowy);
                                                var input = Utilities.ValidateInput(new int[] { 1, 2, 3, 4, 5, 9 });
                                                switch (input)
                                                {
                                                    case (int)MenuPrzelewKrajowy.setOwnAcc:
                                                        {
                                                            //Console.Clear();
                                                            Console.WriteLine("\n\n");
                                                            for (int y = 1; y < listaRachunkow.Where(Acc => Acc.AccType == AccType.ROR).Count() + 1; y++)
                                                            {
                                                                inputArray[y - 1] = y;
                                                            }
                                                            listaRachunkow.WyswietlRachunki(true, AccType.ROR);
                                                            var input2 = Utilities.ValidateInput(inputArray);
                                                            TransferKrajowy.TrnOwnAccNumber = listaRachunkow[input2 - 1].AccNumber;
                                                            break;
                                                        }
                                                    case (int)MenuPrzelewKrajowy.setBnfAcc:
                                                        {
                                                            Console.WriteLine("\n\n");

                                                            //   Console.Clear();
                                                            Console.WriteLine("Wpisz rachunek odbiorcy:");
                                                            var rachunekOdbiorcy = Console.ReadLine();

                                                            if (Utilities.ValidateAccNumber(rachunekOdbiorcy))
                                                            {
                                                                TransferKrajowy.BnfAccNumber = rachunekOdbiorcy;

                                                            }

                                                            break;
                                                        }
                                                    case (int)MenuPrzelewKrajowy.setAmount:
                                                        {
                                                            Console.WriteLine("\n\n");

                                                            Console.WriteLine("Podaj kwotę:");
                                                            TransferKrajowy.TrnAmount = Utilities.ValidateInput();

                                                            break;
                                                        }

                                                    case (int)MenuPrzelewKrajowy.setBnfAddr:
                                                        {
                                                            Console.WriteLine("\n\n");

                                                            Console.WriteLine("Podaj nazwę odbiorcy:");
                                                            TransferKrajowy.BnfName = Console.ReadLine();
                                                            break;
                                                        }
                                                    case (int)MenuPrzelewKrajowy.executeTrn:
                                                        {
                                                            if (TransferKrajowy.TrnOwnAccNumber == null ||
                                                                TransferKrajowy.BnfAccNumber == null ||
                                                                TransferKrajowy.TrnAmount == 0 ||
                                                                TransferKrajowy.BnfName == null)
                                                            {
                                                                Console.WriteLine("Uzupełnij wszystkie pola!");
                                                                Console.ReadKey(true);
                                                                break;
                                                            }
                                                            else
                                                            {
                                                                if (TransferKrajowy.WykonajPrzelew(listaRachunkow, historiaOperacji))
                                                                {
                                                                    Console.ReadKey(true);
                                                                    przelewMenu = false;
                                                                }
                                                                Console.ReadKey(true);
                                                            }

                                                            break;
                                                        }

                                                    case (int)MenuPrzelewKrajowy.back:
                                                        {
                                                            przelewMenu = false;
                                                            break;

                                                        }


                                                }





                                            }
                                            break;
                                            #endregion
                                        }

                                    case 3:
                                        {
                                            PrzelewyMenu = false;
                                            break;
                                        }

                                }

                            }
                            break;
                            #endregion
                        }
                    case (int)Menu.Historia: //TODO: wyszkukiwanie po np. tytule, rachunku
                        {
                            #region Historia
                            bool HistoriaMenu = true;
                            while (HistoriaMenu)
                            {
                                Console.Clear();
                                Console.WriteLine("Historia Operacji:");
                                Console.WriteLine("##########################################");
                                historiaOperacji.WyswietlHistorieOperacji();
                                Console.WriteLine("##########################################");
                                Console.WriteLine("1. Zapis do pliku");
                                Console.WriteLine("5. Powrót do Menu");
                                var input = Utilities.ValidateInput(new int[] { 1,5 });
                                if (input == 5)
                                {
                                    HistoriaMenu = false;

                                }
                                if(input == 1)
                                {
                                    if (historiaOperacji.ZapiszDoPliku())
                                    {
                                        Console.WriteLine("Zapis Udany");
                                        Console.ReadKey(true);
                                    }
                                }


                            }


                            break;
                            #endregion
                        }

                    case (int)Menu.Lokaty:
                        {
                            #region Lokaty
                            bool MenuLokaty = true;

                            bool boolKwota = true;
                            double kwota = 0;

                            while (MenuLokaty)
                            {

                                Console.Clear();
                                listaRachunkow.WyswietlRachunki(AccType.Lokata);
                                Utilities.PokazMenuLokat();
                                var input = Utilities.ValidateInput(new int[] { 1, 2, 3 });

                                switch (input)
                                {
                                    case 1:
                                        {
                                            #region TworzenieLokaty

                                            boolKwota = true;
                                            Console.Clear();
                                            Console.WriteLine("Nowa Lokata:");
                                            var Lokata = new Lokata(AccType.Lokata);
                                            int[] inputArray = new int[listaRachunkow.Where(Acc => Acc.AccType == AccType.ROR).Count()];
                                            for (int y = 1; y < listaRachunkow.Where(Acc => Acc.AccType == AccType.ROR).Count() + 1; y++)
                                            {
                                                inputArray[y - 1] = y;
                                            }
                                            Console.WriteLine("Podaj rachunek źródłowy:");
                                            listaRachunkow.WyswietlRachunki(true, AccType.ROR);
                                            var input2 = Utilities.ValidateInput(inputArray);
                                            var sourceAccount = listaRachunkow[input2 - 1].AccNumber;
                                            Console.WriteLine("Podaj kwotę:");
                                            while (boolKwota)
                                            {
                                                kwota = Utilities.ValidateInput();
                                                if (listaRachunkow.First(p => p.AccNumber == sourceAccount).AccBalance >= kwota)
                                                {
                                                    boolKwota = false;

                                                }
                                                else
                                                {
                                                    Console.Write("Za mało siana na koncie źródłowym");
                                                }

                                            }
                                            Console.WriteLine("Podaj okres lokaty:");
                                            Console.WriteLine("1. {0}", DepositType.Trzymiesieczna);
                                            Console.WriteLine("2. {0}", DepositType.Polroczna);
                                            Console.WriteLine("3. {0}", DepositType.Roczna);
                                            input2 = Utilities.ValidateInput(new int[] { 1, 2, 3 });
                                            Lokata.DepositType = (DepositType)input2 - 1;
                                            Lokata.DepositAccNumber = sourceAccount;
                                            var Przelew = new Przelew();
                                            Przelew.BnfAccNumber = Lokata.AccNumber;
                                            Przelew.BnfName = "Przelew na Lokatę";
                                            Przelew.TrnAmount = kwota;
                                            Przelew.TrnOwnAccNumber = sourceAccount;
                                            listaRachunkow.Add(Lokata);
                                            Przelew.WykonajPrzelew(listaRachunkow, historiaOperacji);


                                            Console.WriteLine("Lokata Utworzona");
                                            break;
                                            #endregion
                                        }
                                    case 2:
                                        {
                                            #region UsuwanieLokaty
                                            Console.Clear();
                                            Console.WriteLine("Którą lokatę chcesz zamknąć?");
                                            Dictionary<int, Lokata> Lokaty = new Dictionary<int, Lokata>();
                                            var index = 1;

                                            foreach (var item in listaRachunkow)
                                            {
                                                if (item.AccType == AccType.Lokata)
                                                {
                                                    Lokaty.Add(index, (Lokata)item);
                                                    index++;
                                                }
                                            }


                                            foreach (var item in Lokaty)
                                            {
                                                Console.WriteLine("{0}\t{1}\t\t{2}\t\t{3}", item.Key, item.Value.AccNumber, Math.Round(item.Value.AccBalance, 2), item.Value.AccOpenDate);

                                            }

                                            int[] inputArray = new int[Lokaty.Count];
                                            for (int y = 1; y < Lokaty.Count() + 1; y++)
                                            {
                                                inputArray[y - 1] = y;
                                            }

                                            int input3 = Utilities.ValidateInput(inputArray);
                                            var ZwrotZLokaty = new Przelew();



                                            ZwrotZLokaty.BnfAccNumber = Lokaty[input3].DepositAccNumber;
                                            ZwrotZLokaty.BnfName = "Zwrot z lokaty";
                                            ZwrotZLokaty.TrnAmount = Lokaty[input3].AccBalance;
                                            ZwrotZLokaty.TrnOwnAccNumber = Lokaty[input3].AccNumber;
                                            ZwrotZLokaty.WykonajPrzelew(listaRachunkow, historiaOperacji);


                                            Console.ReadKey();

                                            listaRachunkow.Remove(Lokaty[input3]);


                                            break;
                                            #endregion
                                        }
                                    case 3:
                                        {
                                            MenuLokaty = false;
                                            break;
                                        }
                                }





                            }
                            break;
                            #endregion

                        }

                    case (int)Menu.Wyjscie:
                        {
                            czyZostac = false;
                            break;
                        }
                        
                }


            }



        }




        enum Menu
        {
            Rachunki = 1,
            Przelewy = 2,
            Historia = 3,
            Lokaty = 4,
            Wyjscie = 5
        }

        enum MenuPrzelewKrajowy
        {
            setOwnAcc = 1,
            setBnfAcc = 2,
            setAmount = 3,
            setBnfAddr = 4,
            executeTrn = 5,
            back = 9
        }





    }
}
